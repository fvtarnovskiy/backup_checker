import sys

IGNORE_LIST_PATH = None
VM_LIST_PATH = None
BACKUP_INFO_PATH = None
CONFIGURED_VM_OUT_PATH = None
NOT_CONFIGURED_VM_OUT_PATH = None
DUPLICATE_VM_OUT_PATH = None
vm_with_no_configured_backup = [] # List of vm without backup task
vm_with_configured_backup = [] # List of vm configured as planned
vm_with_duplicate_backup = [] # List of vm backuped more than expected

# Class to store information about VM
class virtual_machine:
   def __init__(self, id, name, status, backup_tasks = 0):
        self.id = id
        self.name = name
        self.status = status
        self.backup_tasks = backup_tasks

def main():
    EXPECTED_VAR_AMOUNT = 6

    # Check and load variables 
    init(EXPECTED_VAR_AMOUNT)

    ignore_vm_list = load_exceptions(IGNORE_LIST_PATH)
    vm_list = load_vm_list(ignore_vm_list)
    vm_in_jobs_list = load_vm_in_jobs()
    processed_vm_list = process_VMs(vm_list, vm_in_jobs_list)
    split_vm_by_group(processed_vm_list)
    
    print_result(vm_with_configured_backup, "\nVM with correct configuration:")
    print_result(vm_with_no_configured_backup, "\nVM without backup configuration:")
    print_result(vm_with_duplicate_backup, "\nVM in several backup tasks:")
    
    # Save result to files
    write_file(CONFIGURED_VM_OUT_PATH, vm_with_configured_backup)
    write_file(NOT_CONFIGURED_VM_OUT_PATH, vm_with_no_configured_backup)
    write_file(DUPLICATE_VM_OUT_PATH, vm_with_duplicate_backup)

#
# Script init section
#
def init(expected_var_amount):
    # Use global variable
    global IGNORE_LIST_PATH
    global VM_LIST_PATH
    global BACKUP_INFO_PATH
    global CONFIGURED_VM_OUT_PATH
    global NOT_CONFIGURED_VM_OUT_PATH
    global DUPLICATE_VM_OUT_PATH

    # Check var amount
    var_amount = len(sys.argv) - 1
    if (var_amount) < expected_var_amount:
        raise EnvironmentError("Some PATH variables not found ", str(var_amount) + "/" + str(expected_var_amount))
    elif (var_amount) > expected_var_amount:
        raise EnvironmentError("Too many arguments ", str(var_amount) + "/" + str(expected_var_amount))

    # Fill variables
    for i in range(1, len(sys.argv)):
        print('argument:', i, 'value:', sys.argv[i])

    IGNORE_LIST_PATH = sys.argv[1]
    VM_LIST_PATH = sys.argv[2]
    BACKUP_INFO_PATH = sys.argv[3]
    CONFIGURED_VM_OUT_PATH = sys.argv[4]
    NOT_CONFIGURED_VM_OUT_PATH = sys.argv[5]
    DUPLICATE_VM_OUT_PATH = sys.argv[6]
    
#
# Exceptions
#
def load_exceptions(ignore_list_path):
    # Create list to store VMs, that should be ignored
    ignore_vm_list = []

    # Fill ignore list with VM id
    print("Loading the ignore_vm_list..", end="")
    with open(ignore_list_path) as file:
        for line in file:
            vmid = line.strip()
            ignore_vm_list.append(vmid)
    print(" Done")

    return ignore_vm_list

#
# VM list
#
def load_vm_list(ignore_vm_list):
    # Create list to store VMs
    vm_list = []

    # Create list to store info about VM
    vm_info = []

    # Check line for vmid that should be ignored
    print('Prepearing the vm list..', end="")
    def isIgnored(line):
        for ignored_vm in ignore_vm_list:
            if str(ignored_vm) in line:
                print('Machine with id ' + str(ignored_vm) + ' found in exception list. Skipping..')
                return True
        return False
    print(" Done")

    # Appending instances to list
    print('Loading the vm list..')
    #r"C:\Users\OMEN\Desktop\courses\Python\VMs.txt"
    with open(VM_LIST_PATH) as file:
        for line in file:
            # Check line for vmid that should be ignored
            if isIgnored(line):
                continue

            # Fill list with new objects
            # Expected string format: id name status
            vm_info = line.strip().split()
            vm_list.append(virtual_machine(id=vm_info[0],
                                            name=vm_info[1],
                                            status=vm_info[2]))
            print('New object created ' + str(vm_info))

    return vm_list

#
# Scheduled tasks
#
def load_vm_in_jobs():
    # Create list to store all VM entry in scheduled tasks
    vm_in_jobs_list = []

    # Find vmid in all backup tasks and add to vm_in_jobs_list
    print('Prepearing the list of vm in scheduled tasks..')
    #r"C:\Users\OMEN\Desktop\courses\Python\backup_tasks.txt"
    with open(BACKUP_INFO_PATH) as file:
        for line in file:
            # Check only line with 'vmid'
            if 'vmid' in line:
                # Remove 'vmid' from the line
                # Save all id as a list
                vm_IDs = (line.replace('vmid ', '')
                            .strip()
                            .split(','))
                for vm_id in vm_IDs:
                    vm_in_jobs_list.append(vm_id)
    
    return vm_in_jobs_list

#
# Result calculation
#

# Increase backup_tasks for each virtual_machine in vm_in_jobs_list
def process_VMs(vm_list, vm_in_jobs_list):
    print('Modify the vm list..')
    for vm_in_jobs in vm_in_jobs_list:
        for vm in vm_list:
            if vm_in_jobs == vm.id:
                vm.backup_tasks += 1
                print('Scheduled task for VM ' + str(vm.id) + ' found. backup_tasks value changed to ' + str(vm.backup_tasks))
    
    return vm_list


def split_vm_by_group(processed_vm_list):
    global vm_with_no_configured_backup
    global vm_with_configured_backup
    global vm_with_duplicate_backup

    # Separate vm by backup_tasks
    print('Split vm list to groups..', end="")
    for vm in processed_vm_list:
        if vm.backup_tasks < 1:
            vm_with_no_configured_backup.append(vm)
        elif vm.backup_tasks == 1:
            vm_with_configured_backup.append(vm)
        else :
            vm_with_duplicate_backup.append(vm)
    print(" Done")

#
# Print results
#
def print_result(item_list, message):
    print(message)
    print('ID NAME TASKS')
    for vm in item_list:
        print(vm.id, vm.name, vm.backup_tasks)

#
# Save results to files
#

# Writes list of items in specified file
def write_file(file_path, vm_list):
    print('Writing to ' + file_path, end="..")
    with open(file_path, 'w') as file:
        for vm in vm_list:
            s = vm.id, 
            vm.name,
            vm.backup_tasks
            file.write("%s\n" % s)
        print(' Done')

# Run main function
main()