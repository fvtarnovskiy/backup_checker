Ansible-playbook для проверки наличия серверов в задачах автоматического бэкапирования на серверах Proxmox VE.

Для работы используются ansible-playbook(playbook.yaml) и python-script(main.py)
    Основные этапы
        1. Сбор и сохранение информации о виртуальных машинах в кластере
        2. Сбор и сохранение информации о запланированных бэкапах в кластере
        3. Создание временных файлов для храения собранной информации
        4. Заполнение списка игнорируемых машин из переменных
        5. Запуск scripts/main.py для обработки собранных данных
            5.1 Инициализация и проверка переменных
            5.2 Загрузка списка исключений
            5.3 Загрузка списка виртуальных машин
            5.4 Загрузка списка виртуальных машин с запланированным бэкапом 
            5.5 Поиск машин без бэкапа или с повторными бэкапами
            5.6 Распределение виртуальных машин по группам (с бэкапом, без бэкапа, дублирование бэкапа)
            5.7 Сохрание результата во врменные файлы
        6. Проверка результатов scripts/main.py
        7. Отправка сообщения на почту при обнаружении отклонений
        8. Удаление временных файлов 

Подготовка:
    Необходимые утилиты:
        apt install python3
        apt install ansible
        apt install sshpass
        ansible-galaxy collection install community.general
        apt install git
    Наличие ssh-ключей для аутентификации на целевых серверах.
    Скрипт должен быть запущен из-под пользователя с повышенными привилегиями.
    Пользователи, для подключения должны обладать правами для выполнения команд
        qm list | awk '{if(NR>1)print $1, $2, $3}'
        cat /etc/pve/jobs.cfg
    
Запуск скрипта:
    Скачивание скрипта:
        git clone git@gitlab.com:fvtarnovskiy/backup_checker.git
    Используемые переменные:
        ignoreVMs - машины, которые не стоит учитывать (перечисляются через \n)
        smtpHost - адрес почтового сервера для отправки результата на почту
        smtpHostPort - порт для подключения к smtpHost
        fromEmail - почта, от имени которой будет отправлено сообщение
        fromEmailPassword - пароль от fromEmail
        toEmail - почта, на которую нужно отправить уведомление
    Пример inventory:
        [proxmoxCluster]
        server1 ansible_user=root
        server2 ansible_user=root
    Пример команды для запуска скрипта
        ansible-playbook -i ./inventory.yaml playbook.yaml --extra-vars="ignoreVMs='0\n1' smtpHost='smtp.example.com' smtpHostPort='465' fromEmail='exmaple@exmaple.com' fromEmailPassword='***' toEmail='to-exmaple@example.com'"